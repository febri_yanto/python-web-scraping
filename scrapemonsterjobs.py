#!/usr/bin/env python3
import argparse
from bs4 import BeautifulSoup
import json
import math
import requests

from common.Job import Job

FIRST_PAGE_KEY = 'stpage'
LAST_PAGE_KEY = 'page'
LOCATION_KEY = 'where'
JOBS_PER_PAGE = 26
MONSTER_SEARCH_URL = 'https://www.monster.com/jobs/search/'
QUERY_KEY = 'q'

def calculateNumberOfPagesRequired(numberOfJobs):
    """Calculates how many pages are required to be searched to load
    the specified number of jobs

    @param 1 - the number of jobs to load
    """
    return math.ceil(numberOfJobs / JOBS_PER_PAGE)

def getBeautifulSoup(url):
    """Gets the BeautifulSoup object for a given URL

    @param 1 - The URL to get the BeautifulSoup object for
    """
    page = requests.get(url)
    return BeautifulSoup(page.content, 'html.parser')

def getJobs(root_elem, numberOfJobs):
    """Get a list of jobs from a search in monster jobs

    @param 1 - The root element of the page, a BeautifulSoup object
    @param 2 - The number of jobs to get
    """
    job_elems = root_elem.find_all('section', class_='card-content')

    foundJobCount = 0
    for job in job_elems:
        if foundJobCount >= numberOfJobs:
            break
        title_elem = job.find('h2', class_='title')
        company_elem = job.find('div', class_='company')
        location_elem = job.find('div', class_='location')
        if None in [title_elem, company_elem, location_elem]:
            continue
        link_elem = title_elem.find('a')
        foundJobCount += 1
        yield Job(title_elem.text.strip(), link_elem.get("href"),\
            company_elem.text.strip(), location_elem.text.strip())

def getFullUrl(query, location, numberOfJobs):
    """Get the full search URL for a query and location

    @param 1 - The seacrch query
    @param 2 - The location to search for
    @param 3 - The number of jobs needed
    """
    fullUrl = "{0}?{1}={2}&{3}={4}".format( \
        MONSTER_SEARCH_URL, QUERY_KEY, query, LOCATION_KEY, location)
    numberOfPagesRequired = calculateNumberOfPagesRequired(numberOfJobs)
    if numberOfPagesRequired > 1:
        fullUrl = fullUrl + "&{0}=1&{1}={2}".format( \
            FIRST_PAGE_KEY, LAST_PAGE_KEY, numberOfPagesRequired)
    return fullUrl

def searchMonsterJobs(searchString="Software-Developer", location="Philadelphia_2C-PA", numberOfJobs=JOBS_PER_PAGE):
    """The main function for this script. It will search on monster jobs for jobs 
    matching a given search string and location. It will output the results to a
    dictionary.

    @param 1 - The string to search for
    @param 2 - The location to search in
    @param 3 - The number of jobs to search for

    @return - Dictionary containing the specified number of jobs
    """
    # Get a parsable HTML page
    url = getFullUrl(searchString, location, numberOfJobs)
    soup = getBeautifulSoup(url)

    # Get dict version of jobs
    jsonObjects = []
    for job in getJobs(soup, numberOfJobs):
        jsonObjects.append(job.toDict())
    
    jsonDict = {"jobs": jsonObjects}

    return jsonDict

if __name__ == "__main__":
    # Parse the command line arguments
    parser = argparse.ArgumentParser(description="Search for jobs on monster")
    parser.add_argument("-o", '--out', help="File to dump json results to")
    parser.add_argument("-q", '--query', default="Software-Developer", help="String to search jobs for")
    parser.add_argument("-l", '--location', default="Philadelphia_2C-PA", help="Location to search jobs for")
    parser.add_argument("-n", '--number', default=26, type=int, help="The number of jobs to find")
    args = parser.parse_args()

    jobs = searchMonsterJobs(args.query, args.location, args.number)
    if args.out is None:
        print(json.dumps(jobs, indent=4))
    else:
        with open(args.out, 'w+') as f:
            json.dump(jobs, f, indent=4)